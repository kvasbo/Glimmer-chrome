var firstPagePosts = {};
firstPagePosts.posts = [];
firstPagePosts.refreshed = 0;

//var messages = {};
//messages.posts = [];
//messages.refreshed = 0;

var messageThreads = {};
messageThreads.posts = [];
messageThreads.refreshed = 0;
messageThreads.unread = 0;

var streamPosts = {};
streamPosts.posts = [];
streamPosts.refreshed = 0;


var goodImages = [];
var analysedImages = {};

//var events = {};
//events.refreshed = 0;
//events.posts = [];

var myEvents = {};
myEvents.refreshed = 0;
myEvents.posts = [];

var refreshInterval = 5; //Refresh interval in minutes. Must be more than five. 

//var msgTimer;
var eventTimer;
var calTimer;
var firstTimer;
var streamTimer;
var imageTimer;
var imageAnalTimer;
var msgThreadTimer;
 
var dStartup = $.Deferred(); 

//Init background.
$(document).ready(function() {
   
  dStartup.done(startup);
  dStartup.fail(failed);
  
  attachListeners();
  
  doVerifyAccess(); //(deferred);
  
 });

function failed()
{
	console.log("Deferred Failed");
}

function startup()
{
	logger("Startup", "Deferred returned hurrah state");
	addAlarms();
	
	//Init db
	doKudosDBInit();
	
	updateCurrentUser();
	
	doReadImageAnalysisFromStorage();
	
	//updateCityList();
	
	doRefreshData();
	
	//trackPageView("background");
	trackEvent("startup", "normal", "ok");
	
}

function attachListeners()
{
	chrome.runtime.onMessage.addListener(
	  function(request, sender, sendResponse) {
	    if (request.action == "log") {
	     	trackEventGoogle(request.cat, request.text, request.value);
	    }
	  });
}

function doReadImageAnalysisFromStorage(){
	chrome.storage.local.get("analysedImages", function(data) {
		if(typeof data.analysedImages == "object")
		{
			analysedImages = data.analysedImages;
		}
	});
 }
 
function doRefreshData()
{
	updateFrontpageData(1); // as this is a check, we only do 1st page. 
	updateStreamData(1);
	updateEvents(1);
	//updateCalendar(1, "Oslo");
	updateMessageThreads(1);

	doUpdateKudosStats();
	
	getAllKudos(false);
		
	logger("debug", "Refresh", "Refreshing all data");
}


function doRefreshStorageFirstPage()
{
	chrome.storage.local.set({"firstpage": firstPagePosts});
	
	logger("debug", "Refresh", "Refreshing storage - Front page");
}

function doRefreshStorageStream()
{
	chrome.storage.local.set({"stream": streamPosts});
	logger("debug", "Refresh", "Refreshing storage - Stream");
}

function doRefreshStorageMessageThreads()
{
	chrome.storage.local.set({"messagethreads": messageThreads});
	logger("debug", "Refresh", "Refreshing storage - Message threads");
}

/*
function doRefreshStorageCalendar()
{
	chrome.storage.local.set({"events": events});
	logger("debug", "Refresh", "Refreshing storage - Events");
}
*/

function doRefreshStorageEvents()
{
	chrome.storage.local.set({"myEvents": myEvents});
	logger("debug", "Refresh", "Refreshing storage - My events");
}

function doRefreshAnalysedPicturesStorage()
{
	var now = new Date();
	var killInterval = 60*60*24*7; //How old is a pic before we remove it? A WEEK.
		
	//Do the cleaning loop first
	for(i in analysedImages)
	{
		var lastUsed = new Date(analysedImages[i].touched);
		var timeDiff = Math.round((now - lastUsed) / 1000);
		if(timeDiff > killInterval) 
		{
			delete analysedImages[i]; // Remove if too old
			logger("debug", "Images", "image "+i+" removed");
		}
		
	}
	
	chrome.storage.local.set({"analysedImages": analysedImages});
	logger("debug", "Refresh", "Refreshing storage - Picture analysis");
}


/**
Initialise OAuth connection and store the currently logged in user to localstorage for use in the app
*/
function doVerifyAccess(){

logger("Startup", "Verifying access", "");

$.oajax({
       url: "http://underskog.no/api/v1/users/current",
       jso_provider: "underskog",
       jso_scopes: false,
       jso_allowia: true,
       dataType: 'json',
       success: function(data) {
			setCurrentUser(data.data);
			trackEvent("oauth", "access", "ok");
			//console.log("funka");
			dStartup.resolve(); 
       },
       error: function(a,b,c) {
       		//We could not load - flush and try again
       		jso_wipe();
       		trackEvent("oauth", "access", "failed");
       		jso_ensureTokens({"underskog": false});
       		dStartup.reject();
       }
   });
   
}

function updateCurrentUser()
{
	$.when(ropISkogen("users/current")).done(function(results){	
		setCurrentUser(results);
	});
}

/**
Store logged on user to local storage.
*/
function setCurrentUser(data)
{
	var user = {};
	
	user.name = data.name;
	user.realname = data.realname;
	user.id = data.id;
	user.url = data.url;
	user.image = data.image_url;
	user.mail = data.email;
	
	//console.log(user);
	chrome.storage.local.set({'user':user}, function() {
		logger("debug", "OAuth", "User stored");
	});
}

/**
Updates first page post list for the given number of pages. If "force" is set, a full refresh is done.
*/
function updateCityList()
{
	
	var cities = [];
	
	$.when(ropISkogen("cities")).done(function(results){
		var posts = [];
		$.each(results, function(item, msg){
				cities.push(msg);
				//firstPagePosts.posts.push(parsePost(msg, "created_at"));
		 });
		
		if(cities.length > 1)
		{
			chrome.storage.local.set({"cities": cities});
			logger("debug", "Refresh Refreshing storage", "Cities");
		}
		else {
			cities = ["oslo"];
			chrome.storage.local.set({"cities": cities});
			logger("debug", "Refresh Refreshing storage", "Cities (default set)");
		}
		
	}); 
		
}

function doUpdateKudosStats()
{	
	var now = new Date();
	
	//Siste 10
	var kall = "kudos/received?page="+1;
	$.when(ropISkogen(kall)).done(function(results){
			if(results.length > 0 && typeof(results[9] !== "undefined"))
			{
				var then = new Date(results[9].created_at);
				var minutesPerKudos10 = Math.round((now - then) / 600000);
				setCookie("last10freq", minutesPerKudos10);				
			//	console.log("10", then,  minutesPerKudos);
			}
			else
			{
				setCookie("last10freq", 0);
			}		      	
		}); 
		
	//Siste 100
	var kall = "kudos/received?page="+4;
	$.when(ropISkogen(kall)).done(function(results){
			if(results.length > 0 && typeof(results[9] !== "undefined"))
			{
				
				var then = new Date(results[9].created_at);
				var minutesPerKudos100 = Math.round((now - then) / 6000000);
				setCookie("last100freq", minutesPerKudos100);									
			//	console.log("100", then,  minutesPerKudos);
			}
			else
			{
				setCookie("last100freq", 0);
			}			
		}); 
		
	//Siste 1000
	var kall = "kudos/received?page="+34;
	$.when(ropISkogen(kall)).done(function(results){
			if(results.length > 0 && typeof(results[9] !== "undefined"))
			{
				var then = new Date(results[9].created_at);
				var minutesPerKudos1000 = Math.round((now - then) / 60000000);
				setCookie("last1000freq", minutesPerKudos1000);						
				
			//	console.log("1000", then, minutesPerKudos);
			}
			else
			{
				setCookie("last1000freq", 0);
			}
		}); 
}

/**
Updates first page post list for the given number of pages. If "force" is set, a full refresh is done.
*/
function updateFrontpageData(numberofpages)
{
	
	//var d = $.Deferred();
	
	firstPagePosts.refreshed = new Date().getTime();
	firstPagePosts.posts = [];
	
	var i = 1;
	for(i=1;i<=numberofpages;i++)
	{
		$.when(ropISkogen("streams/posts?page="+i)).done(function(results){
			var posts = [];
			$.each(results, function(item, msg){
	    	      	firstPagePosts.posts.push(parsePost(msg, "created_at"));
	      	 });
	      	
	      	clearTimeout(firstTimer);
	      	//clearTimeout(imageTimer);
	      	firstTimer = window.setTimeout(doRefreshStorageFirstPage, 1000); //Ugly hack!
	      //	imageAnalTimer = window.setTimeout(doImageAnalysisLoop, 5000); //Ugly hack!
	      	
		}); 
		
	}
}


/**
Update stream of posts from API. If "force" is true, old existing data will be overwritten (but there will not be a flush).
*/
function updateStreamData(numberofpages)
{
	
	streamPosts.posts = [];
	streamPosts.refreshed = new Date().getTime();
	
	for(i=1;i<=numberofpages;i++)
	{
		$.when(ropISkogen("streams/starred?page="+i)).then(function(results){
			var posts = [];
			
			$.each(results, function(item, msg){
			   
			      if(typeof(msg.bulletin) !== 'undefined')
			      {
			      		streamPosts.posts.push(parsePost(msg.bulletin, "created_at"));
			      }
			      else if(typeof(msg.event) !== 'undefined'){
			      		streamPosts.posts.push(parseEvent(msg.event));
			      }
		  	 });
		  	 
			clearTimeout(streamTimer)
			streamTimer = window.setTimeout(doRefreshStorageStream, 2000); //Ugly hack!
		  	 
		});
	}
		

}


function updateMessageThreads(numberofpages)
{
	messageThreads.refreshed = new Date().getTime();
	messageThreads.unread = 0;
	messageThreads.posts = [];
	
	for(i=1;i<=numberofpages;i++)
	{
		$.when(ropISkogen("messages/conversations?page="+i)).then(function(results){
		
			$.each(results, function(item, thread){
			     
			     messageThreads.unread += thread.unread_count;
			     
			     var msg = thread.last_message;
			     
			     //msg.numberofmessages = 0;
			     
			     messageThreads.posts.push(thread.last_message);
			     
			     //Add unread counter to badge. This is not the final solution.

			     //console.log(thread);
			   	 //messages.posts[msg.id] = msg;
			       
			   });
			   
			   
			   if(messageThreads.unread > 0) 
			   {
			   	var t = {"text": messageThreads.unread.toString()}
			   	chrome.browserAction.setBadgeText(t);
			   }
			   else {
			   	var t = {"text": ""}
			   	chrome.browserAction.setBadgeText(t);
			   }
			   
			   clearTimeout(msgThreadTimer);
			   msgThreadTimer = window.setTimeout(doRefreshStorageMessageThreads, 1000); //Ugly hack!
			   
		});
		
		
	}
}

/**
Update my events view. Based on new backend. Force always true for now.
*/
function updateEvents(numberofpages)
{
	
	myEvents.refreshed = new Date().getTime();
	myEvents.posts = [];
	
	for(i=1;i<=numberofpages;i++)
	{
		
		$.when(ropISkogen("users/current/events?page="+i)).then(function(results){
			$.each(results, function(item, msg){
		          myEvents.posts.push(msg);
		          
		       });
		
			clearTimeout(eventTimer);
			eventTimer = window.setTimeout(doRefreshStorageEvents, 1000); //Ugly hack!
		
		});
	
	}
	
}


/**
Update calendar for a city. Based on new backend. Force always true for now
*/
function updateCalendar(numberofpages, city)
{

	var cityString = encodeURIComponent(city.toLowerCase());
	
	logger("Debug", "Getting calendar", cityString);

	var d = $.Deferred();

	events.refreshed = new Date().getTime();
	events.posts = [];
	
	for(i=1;i<=numberofpages;i++)
	{

		$.when(ropISkogen("events?city="+cityString+"&page="+i)).then(function(results){
			$.each(results, function(item, msg){
		          events.posts.push(msg);
		         
		       });
		
			clearTimeout(calTimer);
			calTimer = window.setTimeout(doRefreshStorageCalendar, 1000); //Ugly hack!
		
		});
	
	}

}

function parseEvent(p)
{

	var out = {};

	out.type = "event";
	out.title = p.title;
	out.id = p.id;
	out.body = p.body;
	out.bodyText = cleanText(p.body);
	out.when = p.time;
	out.tags = p.tags;
	out.creatorId = p.creator.id;
	
	out.creatorImg = p.creator.image_url
	
	if(out.creatorImg == "http://underskog.no/assets/images/noicon_48.png") 
	{
		out.creatorImg = "pics/invisible-man-50.jpg";
	}
	
	
	out.creatorName = p.creator.name;
	out.venue = p.venue;
	out.forumTitle = "Kalender " + p.city;
	
	analyseImage(p.creator.image_url);
	
	out.images = []
	var e = document.createElement('div');
	
	$(e).html(p.body);
			
	$(e).find("img").load(function(image){
			var src = $(this).attr("src");
			analyseImage(src, false);
			out.images.push(src); 
	});
	
	return out;
}

/**
Parse a post from the API and return a richer version. Also parses the media insanity from Underskog.
*/
function parsePost(p, timeToUse)
{
	var out = {};
	
	out.type = "post";
	
	//console.log(p);
	
	try {
		out.title = p.title;
		out.id = p.id;
		out.body = p.body;
		out.bodyText = cleanText(p.body);
		out.when = p[timeToUse];
		out.tags = p.tags;
		out.creatorId = p.creator.id;
		out.creatorImg = p.creator.image_url
		
		if(out.creatorImg == "http://underskog.no/assets/images/noicon_48.png") 
		{
			out.creatorImg = "pics/invisible-man-50.jpg";
		}
		
		
		out.creatorName = p.creator.name;
		
		out.forumTitle = p.forum.title;
		out.forumId = p.forum.id;
		out.forumId = p.forum.id;out.numberOfComments = p.comment_count;
		out.forumId = p.forum.id;out.following = p.following;
		
		//out.whenPretty = getTimeString(out.when);
		
		out.url = "http://underskog.no/samtale/"+p.id;
		
		out.images = [];
		
		var e = document.createElement('div');
		
		analyseImage(out.creatorImg, false);
	
		$(e).html(p.body);
				
		$(e).find("img").load(function(image){
				var src = $(this).attr("src");
				analyseImage(src, false);
				out.images.push(src); //Do not bring images with ignore tag into post image list. 
		});
		
	}
	catch (err)
	{
		console.log("Error", err);
	}

	return out;
}


function analyseImage(url, force)
{

	
	var hash = md5(url);
	var now = new Date().getTime();
	
	if(typeof(analysedImages[hash]) === 'undefined' || force) {
		
		var image = document.createElement('img'); // Create image
		image.src = url;
		
		$(image).on("load", function(){
			
			var src = $(this).attr("src");
			var hash = md5(src);
			var dominantColor = getDominantColor(this);
			var palette = createPalette(this, 3);
			var when = now;
			
			var outImg = {"src": src, "hash": hash, "palette": palette, "dominantColor": dominantColor, "touched": when, "analysed": when};
			
			logger("Debug", "Analysed image", src);
			
			analysedImages[hash] = outImg;
			
			//Store five seconds after last analysis.
			clearTimeout(imageTimer);
			imageTimer = window.setTimeout(doRefreshAnalysedPicturesStorage, 2000); //Ugly hack!
			
		});	
	}
	else {
		analysedImages[hash].touched = now;
	}	
}

/**
Create events and event handlers for housekeeping.
*/
function addAlarms()
{
	logger("debug", "Startup", "Alarms added");
	//Add refresh alarm 
	var d = new Date();
	var ad = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes()+Math.max(5,refreshInterval), 0);
	
	var aInfo = {}; //new Object();
	aInfo.when = ad.getTime();
	aInfo.periodInMinutes = 5;
			
	chrome.alarms.create("Refresh", aInfo);
	
	//Add alarmlistener
	chrome.alarms.onAlarm.addListener(function(alarm){
		if(alarm.name == "Refresh")
		{
			doRefreshData(); //Add maintain function to loop'
		}
	});
}
