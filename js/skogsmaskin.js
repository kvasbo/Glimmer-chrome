var latestMessageTime = 0;
var latestPostTime = 0;
var latestStreamTime = 0;
var tooltipsArray = [];

/**
Update front page view. Based on new backend.
*/
function updateFrontpage()
{
	var numberToView = 30;
	
	chrome.storage.local.get("firstpage", function(data){
		
		$("#tab_front").html("");
		
		var posts = data.firstpage.posts.slice(0,numberToView);
		
		for(p in posts) {
			if(true || posts[p].images.length <= 0)
			{
				showPost(posts[p], "#tab_front", listPost, true);
			}
			else
			{
				showImagesPost(posts[p], "#tab_front");
			}
		}
		
		$("#tab_front").append(Mustache.render(lesMerLink, {"url": ""}));
	
	});
}

/**
Update stream view. Based on new backend.
*/
function updateStream()
{
	
	
	
	chrome.storage.local.get("stream", function(data){
	
	$("#tab_stream").html("");
	
	var posts = data.stream.posts;
			
	for(p in posts) {
		showStreamPost(posts[p], "#tab_stream", listStream, false);
	}
	
	$("#tab_stream").append(Mustache.render(lesMerLink, {"url": "medlem/samtaler/samtaler"}));
	
	});
	
}

/**
Update message view. Based on new backend.
*/
function updateMessages()
{
	
	chrome.storage.local.get("messagethreads", function(data){


	$("#tab_msg").html("");
	
	//console.log(data);

	var numberToView = 20;
	var posts = data.messagethreads.posts; 
	
	for(p in posts) {
          showMessage(posts[p], "#tab_msg");
       }

	$("#tab_msg").append(Mustache.render(lesMerLink, {"url": "user_message/conversations"}));

	var url = "http://underskog.no/user_message/conversations";
	$(".listMessage").on("click", function(){openLinkInNewOrExistingTab(url)});

});
	
}

/**
Update events view. Based on new backend.
*/
function updateEvents()
{
	
	chrome.storage.local.get("myEvents", function(data){
	
	$("#myev_future").html("");
	$("#myev_past").html("");
	
	//Fix the sorting
	var now = new Date();
	
	var numberToView = 20;
	var posts = data.myEvents.posts.slice(0, numberToView);
	
	var sortedPosts = {};
	sortedPosts.past = [];
	sortedPosts.future = [];
	
	for(p in posts)
	{
		var when = new Date(posts[p].time);
		if(when > now)
		{
			sortedPosts.future.push(posts[p]);
			//console.log("future", now, when);
		}
		else {
			sortedPosts.past.push(posts[p]);
			//console.log("past", now, when);
		}
		
		//console.log(posts[p]);
	}
	
	sortedPosts.future.reverse();
	
	for(p in sortedPosts.future) {
		
          showEvent(sortedPosts.future[p], "#myev_future");
     }
     
     for(p in sortedPosts.past) {
     	
           showEvent(sortedPosts.past[p], "#myev_past");
      }
	      
	  $("#tab_myev").append(Mustache.render(lesMerLink, {"url": "medlem/vis/kvasbo"}));    
	      
	      
	});
	
}

function updateKudos()
{
	var now = new Date();
	var kall = "kudos/received?page="+1;
	var out = "";
	var data = {};
	
	$.when(ropISkogen(kall)).then(function(results){
		
		for(i in results)
		{
			//console.log(results[i]);
			data.tid = getTimeString(results[i].created_at, false);
			data.navn = results[i].creator.name;
			data.snippet = intelligentlyTruncate(cleanText(results[i].subject.label),55,false);
			data.id = results[i].subject.id;
			data.url = results[i].subject.url;
			
			if(data.snippet == "") data.snippet = "bilde/video.";
			
			data.snippet = "for " + data.snippet;
			
			out += Mustache.render(kudosTemplate, data);
		}
		
		$("#kudii").html(out);
		
		$(".kudosLinje").on("click", function(a,b){
			//console.log($(a.target).attr("url"));
			//var url = $(a.target).attr("url");
			//console.log($(a.target).attr("url"));
			//openLinkInNewOrExistingTab($(a.target).attr("url"));
		});
		
		//console.log(out);

	});
	
	//Stats
	var k = [];
	k[10] = getCookie("last10freq");
	k[100] = getCookie("last100freq");
	k[1000] = getCookie("last1000freq");
	
	//Estimere når de ble gitt (ikke rett, men fuck it)
	var then = [];
	if(k[10] != null) then[10] = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes() - (k[10]) * 10);
	if(k[100] != null) then[100] = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes() - (k[100]) * 100);
	if(k[1000] != null) then[1000] = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes() - (k[1000]) * 1000);
	
	var ks = {};
	ks.k_min = k[100];
	ks.dato = new Date(then[100]).toLocaleString();;
	ks.num = "100";
	
	var outStats = Mustache.render(kudosStats, ks);
	
	if(k[100] !== 0 && k[100] != null)
	{
		$("#kudiStatBox").show();
		$("#kudistats").html(outStats);
	}
	else {
		$("#kudiStatBox").hide();
	}
	
	//console.log(k, then, out);
	
	
}

/**
Display post with image
*/
function showImagesPost(post, target) {
	var data = {};	//Init data object
	data.title = post.title;
	data.body = post.body;
	data.id = post.id;
	data.who = post.creatorName;
	data.when = post.when;
	data.url = post.url;
	data.tags = post.tags
	data.body = intelligentlyTruncate(post.bodyText, 500, true);
	data.forum = post.forumTitle;
	
	data.images = post.images;
	
	data.popup = Mustache.render(postTooltip, data);

	
	var selectString = "#listPost_"+post.id;

	var out = Mustache.render(listPostImage, data);
	
	//Add output to page
	$(target).append(out);

	var postImageHash = md5(data.images[0]);
	
	//Populate canvas with images and make ken burns effect
	var imageSett = new Array();
							
	$(selectString).on("click", function(){
		openLinkInNewOrExistingTab(data.url);
	}).css("background-image","url("+data.images[0]+")");
	
	//Add tooltip
	if(data.body.length > 5 || data.tags.length > 0)
	{
		tooltipsArray.push(Tipped.create(selectString, data.popup, tippedOptions ));
	}
}

/**
Display a post in a post list
*/
function showStreamPost(post, target, template, useImageColor) {
	
	var data = {};	//Init data object
		
	data.title = post.title;
	data.body = intelligentlyTruncate(cleanText(post.bodyText), 500,true);
	
	data.creator = post.creatorName;
	data.when = getTimeString(post.when, true);
	data.tags = post.tags;
	data.id = post.id;
	data.forum = post.forumTitle;
	
	//var lastCommentsPage = Math.ceil(post.numberOfComments / 30);
	
	
	
	//console.log("Lastcompage", data.numberOfComments ,lastCommentsPage);
		
	data.url = "http://underskog.no/samtale/"+post.id;
	
	var out = Mustache.render(template, data);
	
	var selStringPost = "#postTitleStream_"+data.id;
	
	//console.log(data);
	
	$(target).append(out);
		
	//Add tooltip
	var placeholder = "<div></div>";
	var isEvent = (post.type == "event") ? true:false;
	var streamTippedOptions = tippedOptions;
	streamTippedOptions.showDelay = 800;
	streamTippedOptions.skin = "postPopup";
	streamTippedOptions.inline = false;
	streamTippedOptions.offset = { x: 0, y: -10 };
	streamTippedOptions.afterUpdate = function(a,b) {
		var id = $(b).attr("postid");
		getLatestCommentPopup(id, 0, $(a), 1, $(b), isEvent);
	};
	
	tooltipsArray.push(Tipped.create(selStringPost, placeholder, streamTippedOptions ));

	$(selStringPost).on("click", function(){
		openLinkInNewOrExistingTab(data.url);
	});
	

}

/**
Display a post in a post list
*/
function showPost(post, target, template, useImageColor) {
	
	
	var data = {};	//Init data object
	data.postImage = "";
		
	data.title = post.title;
	data.body = intelligentlyTruncate(post.bodyText, 500, true);
	
	data.postImage = post.creatorImg;
	
	var postImageHash = md5(data.postImage);
	
	data.listPostImageClass = "postCreatorImage";
	
	data.creator = post.creatorName;
	data.when = getTimeString(post.when, true);
	data.tags = post.tags;
	data.id = post.id;
	data.forum = post.forumTitle;
	
	var popup = Mustache.render(postTooltip, data);
	
	data.url = "http://underskog.no/samtale/"+post.id;
	
	var out = Mustache.render(template, data);
	
	var selStringPost = "#postTitle_"+data.id;
	
	$(target).append(out);
	
	//Get info on cretor image
	if(typeof analysedImages[postImageHash] !== 'undefined' && useImageColor)
	{
		var img = analysedImages[postImageHash].dominantColor;
		
		var color = "rgba("+img[0]+","+img[1]+","+img[2]+",1)";
		
		//var textColor = (luma(img) > 165) ? "#222" : "#EEE";
			
		//$(selStringPost).css("background-color", "rgb(240,240,240)");
		$(selStringPost).css("border-color", color);

		//$(selStringPost).css("color", textColor);
		
		//console.log(color);
		//logger("debug", "Image", postImageHash+" found");
	}
	
	//Add tooltip
	if(data.body.length > 5 || data.tags.length > 0)
	{
		tooltipsArray.push(Tipped.create(selStringPost, popup, tippedOptions ));
	}
	
	$(selStringPost).on("click", function(){
		openLinkInNewOrExistingTab(data.url);
	});
	
}

/**
Display a message in a message list
*/
function showMessage(message, target){
	
	var isItFromMe = (message.from.name === currentUser.name) ? true : false;
	var template;
	
	template = listMessage;
	
	data = {};
	
	if(isItFromMe) {
	 	data.imgFrom = message.to.image_url;
	 	data.userFrom = message.to.name;
	 //	template = listMessageTo;
	 	
	}
	else {
		data.imgFrom = message.from.image_url;
		data.userFrom = message.from.name;
	//	template = listMessageFrom;
	}
	
	data.id = message.id;
	data.when = getTimeString(message.sent_at,true);
	data.message = cleanText(message.body);
	
	var out = Mustache.render(template, data);
	
	var selectString = "#listMessageImgbox_"+data.id;
	
	//
	

	
	$(target).append(out);
	
	//$(selectString).css("background-color", "red");
}

/**
Display an event in an envent list
*/
function showEvent(event, target) {
	var out = "";
	var data = {};
	data.id = event.id;
	data.title = event.title;
	
	data.when = getTimeString(event.time, false);
	
	data.venue = event.venue.title;
	data.tags = event.tags;
	data.body = intelligentlyTruncate(cleanText(event.body), 500, true);
	
	data.url = event.url;
	data.venueUrl = event.venue.url;
	
	data.popup = Mustache.render(postTooltip, data);
	
	var out = Mustache.render(listEvent, data);
	
	//Add to DOM
	$(target).append(out);
	
	//Attach events to clicks
	var selStringEvent = "#eventTitle_"+data.id;
	var selStringVenue = "#eventVenue_"+data.id;
	var selEventInlineTitle = "#eventVenue_"+data.id;
	$(selStringEvent).on("click", function(){openLinkInNewOrExistingTab(data.url)});
	$(selStringVenue).on("click", function(){openLinkInNewOrExistingTab(data.venueUrl)});
		
	//Add tooltip
	if(data.body.length > 5 || data.tags.length > 0)
	{
		tooltipsArray.push(Tipped.create(selStringEvent, data.popup, tippedOptions ));
	}
}

function getLatestCommentPopup(id, numberToUse, whereToPut, pageToGet, tooltipComm, event)
{
	
	var kall =  "";
	
	if(event) {kall = "events/"+id+"/comments/?page="+pageToGet.toString();}
	else { kall = "posts/"+id+"/comments/?page="+pageToGet.toString(); }
	
	//console.log("kall " + kall);
	
	var numToReturn = 1;
	
	$.when(ropISkogen(kall)).then(function(results){
		
		//var use = results[results.length - 1]
		
		var use = results[numberToUse];
		
		var data = {};
		data.body = intelligentlyTruncate(cleanText(use.body),500, true);
		data.by = use.creator.name;
		data.img = use.creator.image_url;
		data.when = getTimeString(use.created_at, true);
		
		body = Mustache.render(steamTooltip, data);
		
		whereToPut.html(body);
		Tipped.refresh(tooltipComm);
	});
}