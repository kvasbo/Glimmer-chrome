var kudosDb = openDatabase('kudosDb', '1.0', 'kudos database', 2 * 1024 * 1024 * 10);

var numberofkudi = 0;
var kMax = 100000;
var iterations = 0;
var goingStrong = true;
var newestKudos = false;

function getAllKudos(force)
{
	logger("Debug", "Getting kudos", new Date().toLocaleTimeString());
	
	//Get newest in db
	kudosDb.transaction(function(tx) {
		
		//Get newest kudos in db
		tx.executeSql('select tid from kudos order by tid desc limit 1 ', [], function (tx, results) {
		
		  var len = results.rows.length;
			
			if(!force && len == 1)
			{
		   	 	newestKudos = results.rows.item(0).tid;
		   	 	logger("Debug", "Newest Kudos", new Date(newestKudos).toLocaleString());
		   	}
		   	else {
		   		newestKudos = false; //Do them all
		   	}
		   	
		   	getKudos(1);
		
		});
	});
}

function getKudos(page)
{		
	if(goingStrong == true && iterations < kMax)
	{	
		var kall = "kudos/received?page="+page;
		$.when(ropISkogen(kall)).then(function(results){
			
			iterations++;
			
			if(results.length == 0) goingStrong = false;
						
			$.each(results, function(key, data) {
						
				var sqlK = 'INSERT INTO kudos (hash, fra , for, tid, aar, mnd, dag, time) VALUES(?,?,?,?,?,?,?,?);';
				var sqlP = 'INSERT INTO person (id, navn, bilde) VALUES(?,?,?)';
				var sqlC = 'INSERT INTO comment (id, title, link, type) VALUES(?,?,?,?)';
				
				var kTime = new Date(data.created_at);
				
				if(kTime.getTime() > newestKudos)
				{
					numberofkudi++;
					if(numberofkudi % 1000 == 0) logger("debug", "Kudos stored", numberofkudi);
					
					kudosDb.transaction(function (tx) {
					  
					  tx.executeSql(sqlK,[data.creator.id+"_"+data.subject.id, data.creator.id, data.subject.id, kTime.getTime(), kTime.getUTCFullYear(), kTime.getUTCMonth(), kTime.getUTCDate(), kTime.getUTCHours()], nullDataHandler, errorHandler);
					  tx.executeSql(sqlP, [data.creator.id, data.creator.name, data.creator.image_url], nullDataHandler, errorHandler);
					  tx.executeSql(sqlC, [data.subject.id, cleanText(data.subject.label), data.subject.url, data.subject.type],  nullDataHandler, errorHandler);
					 
					});	
				}
				else {
					logger("debug", "Overlap found, aborting kudos read", "");
					goingStrong = false;
					return false;
				}
				
			});
			
			getKudos(page+1);
		});
	}
	else {
		
		logger("Debug", "Got "+numberofkudi+" kudos", new Date().toLocaleTimeString());
		goingStrong = true;
		iterations = 0;
		numberofkudi= 0;
	}
}

function kudosDBKill()
{
	kudosDb.transaction(function (tx) {
	  tx.executeSql('DROP TABLE IF EXISTS kudos;');
	  tx.executeSql('DROP TABLE IF EXISTS comment;');
	  tx.executeSql('DROP TABLE IF EXISTS person;');
	});
}
 

function doKudosDBInit()
{
	logger("debug", "Init DB", "");
	kudosDb.transaction(function (tx) {
	  tx.executeSql('CREATE TABLE kudos (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, hash unique, fra, for, tid, aar, mnd, dag, time);');
	  tx.executeSql('CREATE TABLE comment (dbid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id unique, title, link, type);');
	  tx.executeSql('CREATE TABLE person (dbid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id unique, navn , bilde);');
	});
}


function nullDataHandler(transaction, results) { }

function errorHandler(transaction, results) {
	if(results.code !== 6)
	{
		logger("error", "DB Error", results);
	}
	
}