var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-40537158-1']);


(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https://ssl.google-analytics.com/ga.js');
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

function trackEventGoogle(cat, text, val)
{
	
	_gaq.push(['_trackEvent', cat + " (" + chrome.runtime.getManifest().version + ")", text, val]);
	
}

function trackPageViewGoogle(url)
{
	_gaq.push(['_trackPageview', url]);
}