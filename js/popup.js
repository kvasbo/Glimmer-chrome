var tabInUse = null;
var currentUser = null;
var analysedImages = null;

var posts = {};
var stream = {};
var events = {};
var myEvents = {};
var messages = {};

$(document).ready(function() {
  
  //Add listener for storage changes
	chrome.storage.onChanged.addListener(function(changes, areaName) {
		for(key in changes)
		{
			if(key == "messagethreads") updateMessages() ;
			if(key == "firstpage") updateFrontpage();
			if(key == "myEvents") updateEvents();
			if(key == "stream") updateStream();			
		}

	});
  
	$( "#tabs" ).tabs({
	  activate: function( event, ui ) {
	  	var active = $( "#tabs" ).tabs( "option", "active" ); //Get newly active tab
		setCookie("activeTab", active); //console.log(active);
		trackEvent("tab", "selected", active);
	  }
	});
    
   switchToLastUsedTab();
   
   refresh();
    
});

function refresh()
{
	$.when(getCurrentUser()).done(doReadImageAnalysisFromStorage()).done(updateFrontpage,updateStream,updateMessages,updateEvents,updateKudos, trackEvent("popup", "open", null));
	
}

function switchToLastUsedTab()
{
	var tab = getCookie("activeTab");
	
	if(tab != null)
	{
		$( "#tabs" ).tabs( "option", "active", tab );
	}
	
}

function getCurrentUser() {
	
	var d = $.Deferred();
	
	chrome.storage.local.get("user", function(data){
		currentUser = data.user;

		d.resolve();
	});
	
}

function openLocalLink(url) {
	var baseurl = "http://www.underskog.no";
	openLinkInNewOrExistingTab(baseurl+url);
}

function openLinkInNewOrExistingTab(url) {
	var options = {};
	options.url = url;
	chrome.tabs.create(options, function(tab){
		tabInUse = tab;
	});	
}

 function doReadImageAnalysisFromStorage(){
	
	var d = $.Deferred();
	
	chrome.storage.local.get("analysedImages", function(data) {
		if(typeof data.analysedImages == "object")
		{
			analysedImages = data.analysedImages;
			d.resolve();
		}
	});
 }
 

 
 
 
 