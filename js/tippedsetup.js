jQuery.extend(Tipped.Skins, {
  'custom' : {
    background: { color: '#fff', opacity: .95 },
   	border: { size: 7, color: '#000', opacity: .3 },
	radius: { size: 0, position: 'border' },
    shadow: false,
    closeButtonSkin: 'light',
	target: 'mouse',
	hook:   'topmiddle'
  }
});

jQuery.extend(Tipped.Skins, {
  'postPopup' : {
    background: { color: '#fff', opacity: .95 },
   	border: { size: 7, color: '#000', opacity: .3 },
	radius: { size: 0, position: 'border' },
    shadow: false,
    closeButtonSkin: 'light',
	target: 'mouse',
	hook:   'topmiddle',
  }
});


var tippedOptions = {
		skin: 'custom',
		showDelay: 500,
		inline: false,
		fadeOut: 200,
		fadeIn: 400,
		onShow: function(content, element) {
			trackEvent("tooltip", "display", element.id);
		}
	}
	
	var tippedKudosOptions = {
		skin: 'postPopup',
		showDelay: 300,
		inline: false,
		fadeOut: 200,
		fadeIn: 400,
		offset: { x: 0, y: -10 },
		onShow: function(content, element) {
			trackEvent("tooltip", "display", element.id);
		}
	}