var db = openDatabase('kudosDb', '1.0', 'kudos database', 2 * 1024 * 1024 * 10);

var offset = new Date().getTimezoneOffset();

offset = offset / 60;

// Grupp på person
// select fra, count(for) as ant from kudos group by fra order by ant desc limit 100

//Beste mnd
// select aar, mnd, count(*) as tell from kudos group by mnd, aar order by aar desc, mnd desc

//Unike personer
//select aar, count(distinct fra) from kudos group by aar

//select aar, (mnd+1), count(*), count(distinct for) from kudos group by aar, mnd order by aar asc, mnd asc

//Topp 100
////select kudos.for, comment.title, count(*) as tell from kudos, comment where comment.id = kudos.for group by for order by tell desc limit 100

var sql = [];


sql["top100"] = "select kudos.for as for, comment.title as title, comment.type as type, count(*) as tell from kudos, comment where comment.id = kudos.for group by for order by tell desc limit 5";
sql["yearly"] = "select aar, count(*) as tell from kudos group by aar order by aar asc";
sql["monthly"] = "select aar, mnd+1 as mnd, count(*) as tell from kudos group by aar, mnd order by aar asc, mnd asc";
sql["last30"] = "select aar, mnd+1 as mnd, dag, count(*) as tell from kudos group by aar, mnd, dag order by tid desc limit 30";
sql["last24h"] = "select aar, mnd, dag, time, tid, count(*) as tell from kudos group by aar, mnd, dag, time order by tid desc limit 24";

console.log(sql["last24h"]);

$( document ).ready(function() {
	
	//getTop100();
	makeYearStat();
	makeMonthStat();
	makeLast30Stat();
	makeLast24hStat();
});



function makeYearStat()
{
	var yearly = [];
	var years = [];
	
	db.transaction(function (tx) {
	   tx.executeSql(sql["yearly"], [], function(tx, rs){
	      for(var i=0; i<rs.rows.length; i++) {
	         var row = rs.rows.item(i)
	        
	         years.push(row['aar']);
	         yearly.push(row['tell']);
	      	}
	      	
	      	drawYearStat(years,yearly,"chart_yearly");
	      }, errorHandler);
	});		
}

function makeMonthStat()
{
	var yearly = [];
	var years = [];
	
	db.transaction(function (tx) {
	   tx.executeSql(sql["monthly"], [], function(tx, rs){
	      for(var i=0; i<rs.rows.length; i++) {
	         var row = rs.rows.item(i)
	         years.push(row['mnd']+"."+row['aar']);
	         yearly.push(row['tell']);
	      	}
	      	//console.log(years,yearly);
	      	drawMonthStat(years,yearly, "chart_monthly" );
	      }, errorHandler);
	});		
}

function makeLast30Stat()
{
		var yearly = [];
		var years = [];
		
		db.transaction(function (tx) {
		   tx.executeSql(sql["last30"], [], function(tx, rs){
		      for(var i=0; i<rs.rows.length; i++) {
		         var row = rs.rows.item(i)
		         years.push(row['dag']+"."+row['mnd']+"."+row['aar']);
		         yearly.push(row['tell']);
		      	}
		      	
		      	years.reverse();
		      	yearly.reverse();
		     
		      	drawLast30Stat(years,yearly, "chart_last30" );
		      }, errorHandler);
		});		
}

function makeLast24hStat()
{
		var yearly = [];
		var years = [];
		
		db.transaction(function (tx) {
		   tx.executeSql(sql["last24h"], [], function(tx, rs){
		      
		      var scaffold = {};
		      var now = new Date();
		     
		      //Get last 24 hrs 
		      for(i=0; i<24;i++)
		      {
		      		var tmpD = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours() - i, 0,0);
		      		var timeIdent = tmpD.getUTCFullYear()+"_"+tmpD.getUTCMonth()+"_"+tmpD.getUTCDate()+"_"+tmpD.getUTCHours();
		      		scaffold[timeIdent] = 0;
		      		years.push(tmpD.getHours()); //Add to scale
		      }
		      
		   		       
		      for(var i=0; i<rs.rows.length; i++) {
		         
		         var row = rs.rows.item(i)
		     
		         var d = new Date(row['tid']);
		         
		         var ident = row['aar']+"_"+row['mnd']+"_"+row['dag']+"_"+row['time']
		         //console.log("ident", ident);
		         
		         if(typeof(scaffold[ident]) !== "undefined")
		         {
		         	scaffold[ident] = row['tell']; //Set actual value
		         }
	        
		      	}
		      	
		      	$.each(scaffold, function(a,b){
		      		
		      		yearly.push(b);
		      		
		      	});
		      	
		      	years.reverse();
		      	yearly.reverse();
		      
		      	drawLast24hStat(years,yearly, "chart_last24h" );
		      }, errorHandler);
		});		

}

function drawYearStat(labels, data, target)
{
	var chartData = {
		labels : labels,
		datasets : [
			{
				fillColor : "rgba(200,220,200,0.7)",
				strokeColor : "rgba(200,220,200,1)",
				pointColor : "rgba(220,220,220,1)",
				pointStrokeColor : "#000",
				data : data
			}
		]
	}
	
	
var scale = createScale(data, 10);

var options = {bezierCurve : false,pointDot : true,pointDotRadius : 1, scaleFontSize : 10, scaleOverride: true, scaleStartValue: 0, scaleSteps: scale.scaleSteps, scaleStepWidth: scale.scaleStepWidth};

	var ctx = document.getElementById(target).getContext("2d");
	var myNewChart = new Chart(ctx).Line(chartData,options);
}

function drawLast30Stat(labels, data, target)
{
	var chartData = {
		labels : labels,
		datasets : [
			{
				fillColor : "rgba(200,220,200,0.7)",
				strokeColor : "rgba(200,220,200,1)",
				pointColor : "rgba(220,220,220,1)",
				pointStrokeColor : "#000",
				data : data
			}
		]
	}
	
	var scale = createScale(data, 10);
	
	var options = {pointDot : true,pointDotRadius : 1, scaleFontSize : 10, scaleOverride: true, scaleStartValue: 0, scaleSteps: scale.scaleSteps, scaleStepWidth: scale.scaleStepWidth};
	

	var ctx = document.getElementById(target).getContext("2d");
	var myNewChart = new Chart(ctx).Line(chartData,options);
}

function drawMonthStat(labels, data, target)
{
	var chartData = {
		labels : labels,
		datasets : [
			{
				fillColor : "rgba(200,220,200,0.7)",
				strokeColor : "rgba(200,220,200,1)",
				pointColor : "rgba(220,220,220,1)",
				pointStrokeColor : "#000",
				data : data
			}
		]
	}
	
	var scale = createScale(data, 10);
	console.log(scale);
	
	var options = {pointDot : true,pointDotRadius : 1, scaleFontSize : 6, scaleOverride: true, scaleStartValue: 0, scaleSteps: scale.scaleSteps, scaleStepWidth: scale.scaleStepWidth};
	var ctx = document.getElementById(target).getContext("2d");
	var myNewChart = new Chart(ctx).Bar(chartData,options);
}

function drawLast24hStat(labels, data, target)
{
	var chartdata = {
		labels : labels,
		datasets : [
			{
				fillColor : "rgba(200,220,200,0.7)",
				strokeColor : "rgba(200,220,200,1)",
				pointColor : "rgba(220,220,220,1)",
				pointStrokeColor : "#000",
				data : data
			}
		]
	}
	
	var scale = createScale(data, 10);
	
	var options = {pointDot : true,pointDotRadius : 1, scaleFontSize : 10, scaleOverride: true, scaleStartValue: 0, scaleSteps: scale.scaleSteps, scaleStepWidth: scale.scaleStepWidth};
	var ctx = document.getElementById(target).getContext("2d");
	var myNewChart = new Chart(ctx).Line(chartdata,options);
	
}



//select aar, mnd+1, dag, count(*) from kudos group by aar, mnd, dag order by tid desc limit 30 

function getTop100()
{

   var result = [];
   db.transaction(function (tx) {
      tx.executeSql(sql["top100"], [], function(tx, rs){
         for(var i=0; i<rs.rows.length; i++) {
            var row = rs.rows.item(i)
            
            var out = "<p>"+row['type']+" - "+row['tell']+" - "+row['for']+" - "+row['title']+"</p>";
            
            $("#stat-top100").append(out);
            
            result[i] = { type: row['type'],
            			  title: row['title'],
            			  antall: row['tell'],
                          for: row['for']
            }
         }
         //console.log(result);
         }, errorHandler);
   });

}

function errorHandler(trans,err)
{
	console.log(trans,err);
}
/*
* Create a nice looking y scale.
*/
function createScale(values, ticks)
{

	var out = {};
	
	var maxValue = Math.round(Math.max.apply(Math, values)); //Finn maksverdi
	var minValue = 0; //Sett minimum 
	var range = Math.abs(maxValue - minValue); //Finn range
	
	var tmpTick = Math.ceil(range / (ticks-1)); //Midlertidig forskjell
	
	var tick = tmpTick.toPrecision(1) * 1; //Ett signifikant tall
	
	var newTop = tick*ticks; //Finn ny topp (temp)
	
	var diff = newTop - maxValue; //Finn avstand til topp
	
	var diffCorr =  diff % tick; //Modulo for å finne cutoff
	
	var nDiff = diff - diffCorr; //Hvor mange ticks tar vi bort?
	
	var corr = Math.max(Math.ceil(nDiff / tick), 0); //Lag delta for ticks
	
	
	out.scaleStartValue = 0;
	out.scaleStepWidth = tick;
	out.scaleSteps = ticks - corr;
	
	
	
	return out;
}

function log10(val) {
  return Math.log(val) / Math.LN10;
}
