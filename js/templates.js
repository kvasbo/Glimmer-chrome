
var listEvent = "<div class='listBox listEvent' id='eventTitle_{{id}}'><p class='listHeader listEventTitle'><span >{{title}}</span></p><p>{{when}} på <span id='eventVenue_{{id}}'>{{venue}}</span></p></div>";


var listMessage = "<div class='listBox listMessage'><div class='listMessageImgbox' id='listMessageImgbox_{{id}}'><img class='imgFrom' src='{{imgFrom}}'/></div><div class='listMessageTextBox'><p class='listMessageHeader'>{{userFrom}} {{when}}</p><p>{{{message}}}</p></div></div>";

var listPost = "<div class='listBox listPost' id='postTitle_{{id}}'><div class='listPostImgBox'><img class='{{listPostImageClass}}' src='{{postImage}}'/></div><div><p class='listHeader listPostListHeaderTitle'>{{title}}</p><p class='postListHeaderAuthorAndTime'><span class='listPostHeaderWho'>{{creator}} i {{forum}} {{{when}}}</span><br></p></div></div>{{{popup}}}";

var listStream = "<div class='listBox liststream'><p class='listheader' postid={{id}} id='postTitleStream_{{id}}'>{{title}}</p></div>";


var listPostImage = "<div class='listBox listPostImage' id='listPost_{{id}}'><div class='listPostImgBox'><img class='{{listPostImageClass}}' src='{{postImage}}'/></div><div><p class='listHeader listPostListHeaderTitle'>{{title}}</p><p class='postListHeaderAuthorAndTime'><span class='listPostHeaderWho'>{{creator}} i {{forum}} {{{when}}}</span><br></p></div></div></div>";

//Elements


//Tooltips

var postTooltip = "<div id='postInline_{{id}}'><p>{{{body}}}</p><p>{{#tags}}<span class='tag'>{{{.}}}</span>{{/tags}}</p></div>";

var steamTooltip = "<div><img src='{{img}}' height='40' width='40'><p class='streamPopBody'>{{{body}}}</p><p class='streamPopHeader'> {{by}} {{when}}</p></div>";

var lesMerLink = "<div class='lesmer'><a href='http://underskog.no/{{link}}' target='_new'>Les mer på underskog.no</a></div>"

var kudosTemplate = "<p class='kudosLinje' id='kudos_{{id}}' url='{{url}}'><span class='kudosTid'>{{tid}}</span><span class='kudosNavn'>{{navn}}</span><span class='kudosSnippet' >{{snippet}}</span></p>";

var kudosStats = "<p>{{k_min}} minutter pr. kudos siden {{dato}} (siste {{num}} kudos)</p>";