/**
Perform API lookup and return the data
*/
function ropISkogen(kall){
  
  var deferred = $.Deferred();
  var baseURL = "https://underskog.no/api/v1/";
  var url = baseURL + kall;
  
  $.oajax({
	       url: url, 
	       jso_provider: "underskog",
	       jso_scopes: false,
	       jso_allowia: true,
	       dataType: 'json',
	       success: function(data, response) {
			   deferred.resolve(data.data);
			   //logger("debug", "API OK", kall); 
			   //trackEvent("api", "ok", kall);
	       },
	       error: function(a,b,c) {
				deferred.resolve(false);
				logger("error", "API Fail", [a,b,c]);
				//trackEvent("api", "error", kall);
				trackError("Api", b, c);
	       }
	   });
    
  return deferred.promise();
  
}
/*
//Add error handling to window
window.onerror = function(message, url, linenumber) {
 // alert("JavaScript error: " + message + " on line " + linenumber + " for " + url);
 	//trackError("General",linenumber, message);
 	return true;
 	//logger("error", message, linenumber);
}
*/
//Track an event to google if tracker is installed
function trackEvent(a,b,c)
{
	chrome.runtime.sendMessage({action: "log", cat: a , text: b, value: c});	
}

function trackError(where, a, b)
{
	chrome.runtime.sendMessage({action: "log", cat: "Error: " + where, text: a, value: b});
}

function logger(level, category, text){
	var now = new Date();
	var timeString = now.getHours() + ":"+now.getMinutes();
	console.log(timeString, category,text);
}

function reload(){
	chrome.runtime.reload();
}

function getTimeString(time, prettify) {
	
	var date = new Date(time);
	var outString = date.toLocaleString();
		
	var s1 = new Date().getTime();
	var s2 = date.getTime();
	
	diffInMinutes = Math.abs(Math.round((s1-s2)/60000));
	
	if(diffInMinutes > 800)
	{
		shortDate = "";
		if(date.getDate() < 10) shortDate += "0";
		shortDate += date.getDate();
		shortDate += ".";
		if((date.getMonth() + 1 ) < 10) shortDate += "0";
		shortDate += date.getMonth() + 1; //= date.getDate() + "." + date.getMonth();
	}
	else {
		shortDate = "";
		if(date.getHours() < 10) shortDate += "0";
		shortDate += date.getHours() + ":";
		if(date.getMinutes() < 10) shortDate += "0";
		shortDate += date.getMinutes();
		//shortDate = date.getHours() + ":" + date.getMinutes();
	}
	
	//var outString = date.toLocaleString();
	
	if(diffInMinutes < 5) outString = "nå";
	else if(diffInMinutes <= 30) outString = "for " + (diffInMinutes - (diffInMinutes % 5)) + " minutter siden";
	else if(diffInMinutes <= 69) outString = "for " + (diffInMinutes - (diffInMinutes % 10)) + " minutter siden";
	else if(diffInMinutes < 120) outString = "for " + (diffInMinutes - (diffInMinutes % 60)) / 60 + " time siden";
	else if(diffInMinutes < 500) outString = "for " + (diffInMinutes - (diffInMinutes % 60)) / 60 + " timer siden";
	else {
		outString = shortDate;
	}
	

	
	//console.log(diffInMinutes);
	if(prettify){
		return outString;
	}
	else
	{
		return shortDate;
	}
}

function cleanText(text) {
	return text.replace(/<\/?[^>]+(>|$)/g, "").trim();
}

function intelligentlyTruncate(str, limit, addReadMore) {
	
	var bits, i;

	bits = str.split('');
	if (bits.length > limit) {
		for (i = bits.length - 1; i > -1; --i) {
			if (i > limit) {
				bits.length = i;
			}
			else if (' ' === bits[i]) {
				bits.length = i;
				break;
			}
		}
		bits.push('...');
		if(addReadMore) bits.push(' (klikk for &aring; lese mer)');
	}
	return bits.join('');
}

function htmlDecode(input){
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function setCookie(c_name,value)
{
var exdate=new Date();
var exdays = 999999;
exdate.setDate(exdate.getDate() + 100);
var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name)
{
var c_value = document.cookie;
var c_start = c_value.indexOf(" " + c_name + "=");
if (c_start == -1)
  {
  c_start = c_value.indexOf(c_name + "=");
  }
if (c_start == -1)
  {
  c_value = null;
  }
else
  {
  c_start = c_value.indexOf("=", c_start) + 1;
  var c_end = c_value.indexOf(";", c_start);
  if (c_end == -1)
  {
c_end = c_value.length;
}
c_value = unescape(c_value.substring(c_start,c_end));
}
return c_value;
}

function getUrlParams()
{
	var urlparams = [];
	if (location.search) {
	    var parts = location.search.substring(1).split('&');
	
	    for (var i = 0; i < parts.length; i++) {
	        var nv = parts[i].split('=');
	        if (!nv[0]) continue;
	        urlparams[nv[0]] = nv[1] || true;
	    }
	}
	return urlparams;
}

function luma(rgb) // color can be a hx string or an array of RGB values 0-255
{
    return (0.2126 * rgb[0]) + (0.7152 * rgb[1]) + (0.0722 * rgb[2]); // SMPTE C, Rec. 709 weightings
}

function hexToRGBArray(color)
{
    if (color.length === 3)
        color = color.charAt(0) + color.charAt(0) + color.charAt(1) + color.charAt(1) + color.charAt(2) + color.charAt(2);
    else if (color.length !== 6)
        throw('Invalid hex color: ' + color);
    var rgb = [];
    for (var i = 0; i <= 2; i++)
        rgb[i] = parseInt(color.substr(i * 2, 2), 16);
    return rgb;
}